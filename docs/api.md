
# API

Snowdrift ships with a Python module on which the 
snowdrift CLI application is built.
The `snowdrift` Python module can be reused and 
integrated into other applications.
These API docs outline the structure and design of the `snowdrift`
python module.

The following key methods are distributed between
various `*.py` files under the `snowdrift/` module 
directory. Have a look at the `snowdrift/__init__.py`
to keep track of where they are imported from.

## Example
Here is an example performing a snowdrift
calculation on an input GRIB forecast dataset
using the `snowdrift` python module API, 
and saving the results in a 'snowdrift.grb' GRIB output file:

```python
import snowdrift
# get config / overriding with config.yml file
conf = snodrift.getConfig("config.yml")
# load forecast data
data = snowdrift.collectData([grib_files,..], conf['InputParameters'])
snowdrift.summary(data, 'loaded forecast')
# get GRIB template
templ = snowdrift.getTemplateMsg(grib_file1, conf['InputParameters'])
## if have any previous snowdrift output...
assimData = snowdrift.collectAssim(prevSnowDrift.grb, conf['OutputParameters'])
snowdrift.summary(assimData, 'assimilation data')
## else set assimData = None
# process input forecast data
snowdrift.processInputData(data)
snowdrift.summary(data, 'prepared forecast data')
# verify necessary data has been made available
snowdrift.verify(data)
# if verification successful, run snowdrift calculation
#  this operation will populate data with the new snowdrift params
snowdrift.snowdrift(data, config=confg['ModelTuning'], assim=assimData)
snowdrift.summary(data, 'snowdrift result')
# save the snowdrift forecast,
snowdrift.saveGrib(data, 'snowdrift.grb', templ, config=conf['OutputParameters'])
# also, can optionally plot 'drift' results to screen
snowdrift.plot(data, -1, 'drift')
```

## Configuration
### getConfig
```python
def getConfig(file=None)
```
Fetches a snowdrift configuation dictionary,
optionally loadin a config YAML file which overrides
the default configurations defined in `snowdrift/config.py`.
Returns a dictionary that contains the following sections:

 * `InputParameters`
 * `ModelTuning`
 * `OutputParameters`

See [example-config](../usage/#example-config) to read
about snowdrift configuration files.

## Loading Data
### collectData
```python
def collectData(files, config=inputConfig)
```
Collects input data from a list of forecast 
GRIB files based on `InputParameters` config description.
See 'InputParameters` config description in the Usage docs:
[example-config](../usage/#example-config)

### collectAssim
```python
def collectAssim(driftFile, outputConfig)
```
Loads assimilation data from a previous snowdrift GRIB
forecast result. The assimilation data loading requires an
`OutputParameters` config dictionary as argument in order to
identify the correct snowdrift forecast parameters to load.
See 'OutputParameters' config description in the Usage docs:
[example-config](../usage/#example-config).

### summary
```python
def summary(data, title="Data Summary")
```
Prints a summary of this data object to StdOut
with an optional title.

## Snowdrift
### processInputData
```python
def processInputData(data)
```
This method calls `calculateDeps` and then `verify`.
Calculates dependent parameters the
for snowdrift forecast calculation, based
on input data.
This is an in-place operation on the input data dict.

### calculateDeps
```python
def calculateDeps(data)
```
This method attempts to generate extra params from loaded data
that are required by snowdrift algorithm.
This includes combining `wind-u/v` vectors into `wind` speed,
and generating `snowfall` parameter from `snowac`, accumulated
snow fall.
The method operatods on the input data set in-place.

### verify
```python
def verify(data)
```
This method checks that necassary input vars are available
before snowdrift calculation. The method will raise
an `IOError` if any parameters necessary for snowdrift
are missing.

### snowdrift
```python
def snowdrift(data, config=modelTuning, assim=None)
```
Calculates snowdrift parameters based on input forecast data,
and any optional `modelTuning` config section loaded
from a config YAML file with `getConfig()`.

The snodrift method also accepts an assimilation dataset,
which is a previous snowdrift GRIB data results loaded
using `collectAssim()`.  Without an assimilation dataset,
the snowdrift calculation will reset itself on each new
snowdrift run. See Usage instructions on 
[assimilation](../usage/#assimilation) for further information.

Output snowdrift parameters are added to the input data dictionary
in-place.

## Output
### plot
```python
def plot(data, i, param, save=None)
```
Plots parameter `param` at step `i` from input data dict.
If `save` 'file name' is provided, saves image to file instead
of display on screen.

### getTemplateMsg
```python
def getTemplateMsg(file, config)
```
Collect a single appropriate msg from grib forecast,
for later use as template in writing snow drift results.
Requires an input forecast file path and an `InputParameters` config.

### saveGrib
```python
def saveGrib(data, filename, template_msg, config=outputConfig)
```
Saves snowdrift parameters to a new grib file,
appending each message.
Requires a template message from source forecast
in oreder to replace values and write in compatible
GRIB formatting to the source.
