# Development
As a **FOSS** project, anyone can take part in contributing improvements and
features to the Snowdrift module and app.

## Setup
Some development modules and applications are required when working on improving the 
source code. Please review the 
[development installation instructions](../installation/#development-setup).

## Contributing
The easiest way to contribute improvements to the project
is to fork the current repository hosted at
[https://gitlab.com/vedurstofan/snowdrift](https://gitlab.com/vedurstofan/snowdrift).
Check out instructions how to fork on gitlab or github:

 * [forking with github](https://docs.github.com/en/github/getting-started-with-github/fork-a-repo)
 * [forking with gitlab](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork)

Once you have your own fork of the project,

  * create a new issue for the new improvement, [here](https://gitlab.com/vedurstofan/snowdrift/-/issues)
  * create a branch with your new software improvements:

```sh
$ git clone https://...your_fork...
$ git checkout -b <issue_name> # or <improvement_name>
```
At this stage do some code improvements on your new branch,
and commit your changes to your fork using **git** source
code management operations, [see git docs](https://git-scm.com/doc), eg.:

```sh
$ git add [your improved files]
$ git commit
$ git push origin
```

Once you are happy with your improvements, create a merge-request to the main Snowdrift repository, [here](https://gitlab.com/vedurstofan/snowdrift/-/merge_requests)

  * as **source repository** insert the full path to your fork on github or gitlab and select the branch with your improvements. 
  * select `main` as the target branch in the Snowdrift repository.
  * you will have to create an account with gitlab.com in order to complete this step

The admins for the Snowdrift project will get a notification of the **issue** and **pull request**, 
and will review the software changes that you propose.
In code review you may be requested to improve or change some aspects of your new code
before merge with the `main` branch is accepted.

## Docs
The Snowdrift docs are hosted at 
[snowdrift.readthedocs.org](https://snowdrift.readthedocs.org)
During development of the Snowdrift module and app 
you may need to update the documentation with revised
documentation.
The documents are written in Markdown and organized under
the project `docs/` folder.
The documents are compiled with [MkDocs](https://www.mkdocs.org/).

### Updating docs
Start a live MkDocs server while editing the docs,

```sh
$ mkdocs serve
INFO    -  Building documentation... 
INFO    -  Cleaning site directory 
INFO    -  Documentation built in 0.15 seconds 
[I 210402 13:40:34 server:335] Serving on http://127.0.0.1:8000
INFO    -  Serving on http://127.0.0.1:8000
...
```
You can now edit the documentation files under `docs/*.md` and
the server will automatically recompile and update the local
pages in the web-browser under `http://127.0.0.1:8000` during 
editing.

### Deploying docs
The docs are automatically compiled and deployed by
[readthedocs.org](https://readthedocs.org) from the `main`
repository branch. As soon as any new improvements have
been merged with `main`, readthedocs should detect the
changes, compile and issue a new update to the doc pages
at [snowdrift.readthedocs.org](https://snowdrift.readthedocs.org).
**Note: The update may take some minutes to compile and a refresh
of your web-browser.**
