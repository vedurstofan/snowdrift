
# Installation
The software requires `python3` and depending on your
requirements the following steps to setup the software.

## Prouduction setup
```sh
# ECMWF grid decoder library
$ sudo apt-get install libeccodes-dev
```

Install the snowdrift module and cli app with pip,
```sh
$ sudo pip3 install "git+https://gitlab.com/vedurstofan/snowdrift"
```

Check that the snowdrift cli has been installed,
```sh
$ snowdrift -h
```

## Development setup
For software development and testing of the module,
further dependencies may be required:

```sh
# ECMWF grid decoder library and git
$ sudo apt-get install libeccodes-dev git
# python modules from pip
$ sudo pip3 install numpy bcolz pygrib pillow trollimage nose mkdocs more_itertools
```

To work on the snowdrift code, you should clone the software project,
and set the `PYTHONPATH` environment variable
to include the snowdrift module folder:

```sh
$ git clone "https://gitlab.com/vedurstofan/snowdrift"
$ cd snowdrift
$ export PYTHONPATH=$PWD:$PYTHONPATH
$ python3
> import snowdrift
# do some coding...
```
This instructs `python` to import and use this particular snowdrift module that you are working on during development.

## Tests
It is a good idea to run the tests provided with the project in the `tests/` directory.
This helps to confirm that the software is working as intended on your platform.

```sh
$ python3 -m "nose" -v ./tests/

test_input_data_loading (test.Test) ... ok
test_input_data_sorting (test.Test) ... ok
test_process_input_data (test.Test) ... ok
test_snowdrift (test.Test) ... ok
test_config_file (test_config.Test) ... ok
test_config_file_missing_sections (test_config.Test) ... ok
...
```

To capture the standard output from the tests, you can run with `-s` flag,

```sh
python3 -m "nose" -vs ./tests/
```
