
<!-- div style="text-align: right;">
<img src="vi.jpg" style="height: 40px;"/>
<img src="vegsyn.jpg" style="height: 25px;"/>
<img src="VegagerdinLogo.jpg" style="height: 40px;"/>
</div -->

<center>
<img src="snowdriftLogo.png" />
</center>

# About

Snowdrift is a forecast model component for snowdrift prediction.
It implements a novel snow drift forecast algorithm developed and documented
by **Vegsýn ehf. (Dr Skuli Thordarson)** in 2013 and 2014.

The model was first implemented for routine numerical weather prediction 
at the **Icelandic Meteorological Office** in 2014, and has been running
on the Icelandic LAM forecast ever since.

The original work behind the model was financed with grant support from
the **European Northern Perimetry Program (SNAPS project)** and the
**Icelandic Roads and Coastal Administration**. This new software 
implementation of the model has been commissioned by
the **Icelandic Meteorological Office** in order to make the model more
portable and configurable for different model inputs.

The project has been further supported by grant in 2021 
from the **Icelandic Roads and Coastal Administration** as part of
developing a novel road-weather forecast interface for road authorities.

The snowdrift algorithm is documented in the following reports:

 * [International Road Weather Conference report 'English'](https://gitlab.com/vedurstofan/snowdrift/-/blob/1705fea2fca415121dede492dd674cb9cc66bc85/reports/irw_conference_report.pdf)
 * [Roads Authority report, chapter 7 'Icelandic'](https://gitlab.com/vedurstofan/snowdrift/-/blob/main/reports/Snj%C3%B3kort%20af%20%C3%8Dslandi%20og%20skafrenningssp%C3%A1r%20II.pdf)

## Quickstart

install deps, if not already installed:
```sh
$ sudo apt-get install libeccodes-dev git feh
$ sudo pip3 install numpy bcolz pygrib pillow trollimage nose more_itertools
```
get the project and run tests:
```sh
$ git clone "https://gitlab.com/vedurstofan/snowdrift"
$ cd snowdrift
$ export PYTHONPATH=$PWD
# tests data will be downloaded to './tests/testdata/'
$ python3 -m "nose" -v ./tests/
```
generate some snowdrift forecast frames:
```sh
$ bin/snowdrift tests/testdata/harmonie* --out imgs/test --format PNG
# loop through the resulting images,
$ feh -D 0.3 imgs/test*
# generate a snowdrift forecast in GRIB format
$ bin/snowdrift tests/testdata/harmonie* --out snowdrift.grb --format GRIB
```
<center>
<img src="animation.gif" style="width:60%; border: 1px solid lightgray;"/>
</center>

Install the snowdrift module on your platform:
```sh
$ sudo pip3 install .
# OR
$ sudo pip3 install "git+https://gitlab.com/vedurstofan/snowdrift"
# The CLI app should now be available on the command line
$ snowdrift -h
```
