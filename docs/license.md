#License

Copyright (c) 2021 Icelandic Meteorological Office,
Bustadavegi 7-9, 105 Reykjavik, Iceland (http://www.vedur.is)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to use
the Software for non-commercial purposes, including the rights
to copy, modify and merge the Software. No permission is granted to publish, 
distribute, sublicense, and/or sell copies of the Software without expressed 
permission, see below.

The snowdrift algorithm (the "Algorithm") implemented by the Software is 
the Intellectual Property right of Dr. Skuli Thordarson, Vegsyn ehf, 
Nordurbakka 9b, 202 Hafnarfjordur, Iceland. All rights related to use
and applications of the Algorithm for commercial purposes are reserved. 

For commercial use of the Algorithm please notify and request permission 
by email to skuli@vegsyn.is.

For commercial use of the Software please notify and request permission by 
email to fyrirspurnir@vedur.is and skuli@vegsyn.is.

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
