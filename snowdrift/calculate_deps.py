import logging
import numpy as np
# Note we wrap all new parameters in S() when
# storing a handle to the new data
# This ensurs the array data is compressed or memmaped
# if requested...
from .array_store import arrayStore as S

# Calculates dependent parameters the
# for snowdrift forecast calculation, based
# on input data.
# This is an in-place operation on the input data dict
def processInputData(data):
    calculateDeps(data)

    # verify necessary parameters are available,
    # for snowdrift calculation...
    verify(data)

# check that necassary input vars are available,
# for snowdrift calculation...
def verify(data):
    logging.info("verifying loaded data for snowdrift calculation")
    for par in ('snowfall', 'snowground', 'temp', 'wind'):
        if par not in data:
            logging.error(" - FAIL")
            raise IOError("missing %r parameter, please configure InputParameters section in --config config.yml, see docs: https://snowdrift.readthedocs.io/en/latest/usage/#configuring"%par)
    logging.info(" - OK")

# tries to generate extra params from loaded data,
# that are required by snowdrift algorithm...
def calculateDeps(data):
    logging.info("calculating dependent parameters")

    # snow fall rate if not provided,
    calculate_snowfall(data)
    # wind from wind u/v if not provided,
    calculate_wind(data)
 
# calculate snow fall (hourly rate), if not provided
def calculate_snowfall(data):
    if ('snowfall' not in data) and ('snowac' in data):
        logging.info(" - calculating snow fall rate")
        unit = data['snowac']['unit'] + "/h"
        data['snowfall'] = {'times':[], 'values':[], 'unit':unit}
        snowac = data['snowac']['values']
        times = data['snowac']['times']
        n = len(times)
        snowfall = data['snowfall']['values']
        snowfalltimes = data['snowfall']['times']

        # first snow step...
        snowfall.append( S(new_array(data)) ) # zeroed first step
        snowfalltimes.append( times[0] )
        # rest are diff...
        for i in range(1, n):
            # hourly snowfall, so get time step...
            dtHours = (times[i] - times[i-1]).total_seconds()/3600.0
            dSnow = (snowac[i] -  snowac[i-1])/dtHours
            snowfall.append( S(dSnow) )
            snowfalltimes.append(times[i])
        # can delete snowac,
        del data['snowac']

# calculate wind from u/v if necessary,
def calculate_wind(data):
    if 'wind' not in data and (('wind-u' in data) and ('wind-v' in data)):
        logging.info(" - calculating wind speed from u/v")
        unit = data['wind-u']['unit']
        data['wind'] = {'times':[], 'values':[], 'unit':unit}
        windu = data['wind-u']['values']
        windv = data['wind-v']['values']
        times = data['wind-u']['times']
        wind = data['wind']['values']
        windtimes = data['wind']['times']
        n = len(times)
        for i in range(n):
            u = windu[i]
            v = windv[i]
            t = times[i]
            spd = (u**2 + v**2)**0.5
            wind.append( S(spd) )
            windtimes.append(t)
        # can delete u/v wind after wind calculation
        del data['wind-u']
        del data['wind-v']

# get new empty data array to work with,
def new_array(data):
    return data['temp']['values'][0]*0.0
    
# check that necassary input vars are available,
# for snowdrift calculation...
def verify(data):
    logging.info("verifying loaded data for snowdrift calculation")
    for par in ('snowfall', 'snowground', 'temp', 'wind'):
        if par not in data:
            logging.error(" - FAIL")
            raise IOError("missing %r parameter, please configure InputParameters section in --config config.yml, see docs: https://snowdrift.readthedocs.io/en/latest/usage/#configuring"%par)
    logging.info(" - OK")