import logging
import yaml
import collections.abc

# Default config string...
defaultConf = """
InputParameters:
    snowac:
        id: 'heightAboveGround:0:184'
        unit: 'kg m**-2'
    snowground:
        id: 'heightAboveGround:0:65'
    temp:
        id: 'heightAboveGround:0:11'
    wind-u:
        id: 'heightAboveGround:10:33'
    wind-v:
        id: 'heightAboveGround:10:34'

ModelTuning:
    snow_cover_threshold: 1.0
    snow_fall_threshold: 0.1
    wind_speed_normalization: 12.0
    wind_zero_drift: 6.0
    mobility_snowage_reduction:
        snowage: 24
        mobility: 0.6
    mobility_driftac_reduction:
        driftac1: 2.0
        mobility1: 0.6
        driftac2: 6.0
        mobility2: 0.3

OutputParameters:
    type_of_level: 'heightAboveGround'
    level: 0
    snowage_parameter: 145
    driftac_parameter: 146
    mobility_parameter: 147
    drift_parameter: 148
    temp_parameter: 160
    snowground_parameter: 161
    snowfall_parameter: 162
    wind_parameter: 163
"""

def update(d, u):
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = update(d.get(k, {}), v)
        else:
            d[k] = v
    return d

# Loads snowdrift config file, overriding
# default configurations defined in `snowdrift/config.py`
def getConfig(file=None):
    # start from default...
    conf = yaml.safe_load(defaultConf)
    # if config file provided apply updates
    if file:
        with open(file, 'r') as stream:
            try:
                fconf = yaml.safe_load(stream)
            except yaml.YAMLError as e:
                logging.error(e)
                raise IOError("failed to read config %r"%file)
            # if successfully lodaded
            # run deep update conf dict,
            conf = update(conf, fconf)
    return conf

# default data collection config...
config = {
    'snowac': {
        'id': 'heightAboveGround:0:184',
        # override unit if not provided...
        'unit': 'kg m**-2'
    },
    # Snow on ground, keeps track of snow covered
    # and snow free areas.
    'snowground': {
        'id': 'heightAboveGround:0:65'
    },
    # temp should ideally be surface temperature
    # if model has it
    'temp': {
        'id': 'heightAboveGround:0:11',
        #'id': 'heightAboveGround:2:11'
    },
    'wind-u': {
        'id': 'heightAboveGround:10:33'
    },
    'wind-v': {
        'id': 'heightAboveGround:10:34'
    }
}
