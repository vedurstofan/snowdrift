import pygrib
import logging
import numpy as np
from .compress import compress2Ndarray, Compress
from .mmap import MArray

outputConfig = {
    'type_of_level': "heightAboveGround",
    'level': 0,
    'snowage_parameter': 145,
    'driftac_parameter': 146,
    'mobility_parameter': 147,
    'drift_parameter': 148,
    # the following are necessary for assimilation
    # into next forecast
    'temp_parameter': 160,
    'snowground_parameter': 161,
    'snowfall_parameter': 162,
    'wind_parameter': 163,
}

def convertArray(v):
    if isinstance(v, Compress):
        return compress2Ndarray(v)
    elif isinstance(v, MArray):
        # open it first...
        v.open()
        return np.array(v.data)
    # else just return
    return v

# Saves snowdrift parameters to a new grib file,
# appending each message.
# Requires a template message from source forecast
# in oreder to replace values and write in compatible
# GRIB formatting to the source.
def saveGrib(data, filename, template_msg, config=outputConfig, drift_only=False):
    msg = template_msg

    # create file,
    out = open(filename,'wb')

    # get handle on snowdrift parameter data
    times = data['temp']['times']
    n = len(times)
    
    drift = data['drift']['values']
    snowage = data['snowage']['values']
    driftac = data['driftac']['values']
    mobility = data['mobility']['values']
    temp = data['temp']['values']
    snowground = data['snowground']['values']
    snowfall = data['snowfall']['values']
    wind = data['wind']['values']

    # loop through snowdrift results,
    # and write GRIB msgs...
    for i in range(n):
        # drift
        logging.info("writing drift step %d"%i)
        msg.level = config['level']
        msg['indicatorOfParameter'] = config['drift_parameter']
        # NOTE: Forecast time steps may need improvement
        #       for other forecast sources like NCEP.
        #       Keep an eye on this for later improvements if needed.
        msg['startStep'] = i
        msg['endStep'] = i
        vals = drift[i]
        vals = convertArray(vals)
        msg['values'] = vals
        out.write(msg.tostring())

        # snowage
        logging.info("writing snowage step %d"%i)
        msg.level = config['level']
        msg['indicatorOfParameter'] = config['snowage_parameter']
        msg['startStep'] = i
        msg['endStep'] = i
        vals = snowage[i]
        vals = convertArray(vals)
        msg['values'] = vals
        out.write(msg.tostring())

        # driftac
        logging.info("writing driftac step %d"%i)
        msg.level = config['level']
        msg['indicatorOfParameter'] = config['driftac_parameter']
        msg['startStep'] = i
        msg['endStep'] = i
        vals = driftac[i]
        vals = convertArray(vals)
        msg['values'] = vals
        out.write(msg.tostring())

        # mobility
        logging.info("writing mobility step %d"%i)
        msg.level = config['level']
        msg['indicatorOfParameter'] = config['mobility_parameter']
        msg['startStep'] = i
        msg['endStep'] = i
        vals = mobility[i]
        vals = convertArray(vals)
        msg['values'] = vals
        out.write(msg.tostring())

        ## if drift_only.. 
        ## skip extra assimilation parameters below
        if drift_only:
            continue

        # temp
        logging.info("writing temp step %d"%i)
        msg.level = config['level']
        msg['indicatorOfParameter'] = config['temp_parameter']
        msg['startStep'] = i
        msg['endStep'] = i
        vals = temp[i]
        vals = convertArray(vals)
        msg['values'] = vals
        out.write(msg.tostring())

        # snowground
        logging.info("writing snowground step %d"%i)
        msg.level = config['level']
        msg['indicatorOfParameter'] = config['snowground_parameter']
        msg['startStep'] = i
        msg['endStep'] = i
        vals = snowground[i]
        vals = convertArray(vals)
        msg['values'] = vals
        out.write(msg.tostring())

        # snowfall
        logging.info("writing snowfall step %d"%i)
        msg.level = config['level']
        msg['indicatorOfParameter'] = config['snowfall_parameter']
        msg['startStep'] = i
        msg['endStep'] = i
        vals = snowfall[i]
        vals = convertArray(vals)
        msg['values'] = vals
        out.write(msg.tostring())

        # wind
        logging.info("writing wind step %d"%i)
        msg.level = config['level']
        msg['indicatorOfParameter'] = config['wind_parameter']
        msg['startStep'] = i
        msg['endStep'] = i
        vals = wind[i]
        vals = convertArray(vals)
        msg['values'] = vals
        out.write(msg.tostring())

    out.close()