import argparse, glob
import logging
import sys
import os
import time
from .collect_data import collectData, summary, getTemplateMsg
from .calculate_deps import processInputData
from .collect_assim import collectAssim
from .snowdrift import snowdrift
from .plot import plot
from .save_grib import saveGrib
from .config import getConfig
from .array_store import setStore
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.INFO)

# command line app definition
def cli():
    # Setup and parse program arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('forecast_files', help="(required) input NWP forecast in GRIB format", nargs='+')
    parser.add_argument('--config', metavar='FILENAME',
    help="""(optional) config file for configuring the input parameters and
    to adjust tuning of the snowdrift model""", default=None)
    parser.add_argument('--assim', metavar='FILENAME', help="(recommended) assimilate previous snowdrift result into new snowdrift forecast, accepts snowdrift GRIB files from previous run", default=None)
    parser.add_argument('--out', metavar='FILENAME', help="save result to file(s) (can use this, if GRIB format, for --assim on next run)", default=None)
    parser.add_argument('--out-drift', metavar='FILENAME', help="save file(s) with only snowdrift parameters results (can not use this, if GRIB format, for --assim on next run)", default=None)
    parser.add_argument('--format', help="output format ['GRIB' (default) | 'PNG']", default="GRIB")
    parser.add_argument('--show', metavar='STEP', help="show snow drift result at STEP (-1 is last step)", default=None)
    parser.add_argument('--verify', help="only read input data and verify that necessary parameters are available, or derived from input, for snowdrift calculation", action='store_true')
    parser.add_argument('--compress', help="applies data compression, saving on RAM usage", action='store_true')
    parser.add_argument('--mmap', metavar='DIRECTORY', help="work directory for storing data on disk during processing, reducing RAM usage for large forecast models (Note: This overrides the in memory --compress option)", default=None)
    args = parser.parse_args()

    # Target forecast dataset...
    files = args.forecast_files

    # compression?
    compress = args.compress
    # mmap directory?
    mmap = args.mmap
    setStore(mmap=mmap, compress=compress)

    # create memmap directory if not existing
    if mmap is not None:
        try:
            if not os.path.exists(mmap):
                logging.info("creating mmap directory %r"%mmap)
                os.mkdir(mmap)
        except:
            logging.error("failed to create mmap directory %r"%mmap)
            sys.exit(1)

    # load config,
    # will set defaults, if no config file
    config = getConfig(args.config)

    # Load input fc parameter data,
    #  - pass input data config here
    # This also verifies input dataset...
    inputData = None
    assimData = None
    templateMsg = None
    try:
        # load current forecast data...
        inputData = collectData(files, config=config['InputParameters'])
        # collect a template message for later use in writing GRIB
        templateMsg = getTemplateMsg(files[0], config['InputParameters'])
        # post process and verify input data
        # - this attempts to add missing parameters in place
        # - then runs verification if any parameters are
        #   missing for snowdrift calculations.
        processInputData(inputData)
        # print data loading summary
        summary(inputData, title="Loaded Forecast Data")

        # Load previous forecast data if --assim given...
        if args.assim:
            # assimilation data is loaded based on output params in snowdrift config 
            # - i.e. last run output is taken as input for assimilation
            assimData = collectAssim(args.assim, config['OutputParameters'])
            # print assim data summary
            summary(assimData, title="Loaded Assimilation Data")
        else:
            assimData = None
    except Exception as e:
        logging.error(e)
        # exit with error code
        sys.exit(1)

    # if --verify requested, just exit now,
    #    I.e. verification is always run above with error report
    #    This skipps snowdrift calculation below
    if args.verify:
        logging.info("input data succesfully verified")
        sys.exit(0)

    # Run snowdrift calculation on dataset,
    try:
        data = snowdrift(inputData, config=config['ModelTuning'], assim=assimData)
    except Exception as e:
        logging.error(e)
        # exit with error code
        sys.exit(1)

    # print summary,
    summary(data, title="Snowdrift Result")

    # save,
    if args.out:
        filen = args.out
        # save as images...
        if args.format.lower() == 'png':
            logging.info("writing PNG images to %r"%(filen+"*"))
            n = len(data['drift']['times'])
            for step in range(n):
                plot(data, step, 'snowage', save=filen)
                plot(data, step, 'driftac', save=filen)
                plot(data, step, 'mobility', save=filen)
                plot(data, step, 'drift', save=filen)
        elif args.format.lower() == 'grib':
            logging.info("writing snowdrift parameters to GRIB - includes assim parameters%r"%(filen+"*"))
            saveGrib(data, filen, templateMsg, config=config['OutputParameters'])
        else:
            logging.error("unknown output format %r"%args.format)
            sys.exit(1)

    if args.out_drift:
        # this writes only GRIB, only 4x snowdrift params results,
        # does not include extra params for assimilation
        filen = args.out_drift
        logging.info("writing snowdrift parameters to GRIB %r"%(filen))
        saveGrib(data, filen, templateMsg, config=config['OutputParameters'], drift_only=True)

    # Test plot the results at step
    if args.show:
        step = int(args.show)
        plot(data, step, 'snowage')
        plot(data, step, 'driftac')
        plot(data, step, 'mobility')
        plot(data, step, 'drift')
