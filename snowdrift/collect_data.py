import pygrib
import sys
import time
import logging
import numpy as np
from datetime import datetime, timedelta
from more_itertools import sort_together
from .calculate_deps import calculateDeps
from .compress import Compress
from .array_store import arrayStore as S, MArray

# default input config...
inputConfig = {
    'snowac': {
        'id': 'heightAboveGround:0:184',
        # override unit if not provided...
        'unit': 'kg m**-2'
    },
    # Snow on ground, keeps track of snow covered
    # and snow free areas.
    'snowground': {
        'id': 'heightAboveGround:0:65'
    },
    # temp should ideally be surface temperature
    # if model has it
    'temp': {
        'id': 'heightAboveGround:0:11',
        #'id': 'heightAboveGround:2:11'
    },
    'wind-u': {
        'id': 'heightAboveGround:10:33'
    },
    'wind-v': {
        'id': 'heightAboveGround:10:34'
    }
}

# Collects data from a list of forecast 
# GRIB files based on input config.
def collectData(files, config=inputConfig):
    # reverse mapping of config dict for convenience,
    id2Par = {v['id']: k for k, v in config.items()}

    # create data holder,
    data = {}

    for f in files:
        # open the files...
        try:
            G = pygrib.open(f)
        except OSError as e:
            logging.error("failed to open file %r"%f)
            raise e

        logging.info("collecting from %r"%f)

        # scan for data
        for g in G:
            # read the msg identifiers
            toLevel = g['typeOfLevel']
            level = g['level']
            ioPar = g['indicatorOfParameter']
            unit = g['units']
            # construct string unique identity
            id = "%s:%d:%d"%(toLevel, level, ioPar)

            # check against config,
            if id in id2Par:
                # collect this data,
                par = id2Par[id]
                if par not in data:
                    logging.info(" - found %r"%par)
                    # check if input config overrides unit
                    if config[par].get('unit'):
                        unit = config[par].get('unit')
                    # create data entry for this par...
                    data[par] = {'times': [], 'values':[], 'unit': unit}

                # pick out data...
                vals = g.values.astype(np.float32)

                # convert any masked array data...
                if hasattr(vals, 'mask'):
                    vals = np.ma.getdata(vals)

                # convert units if necessary,
                if unit == 'K':
                    vals -= 273.15
                    data[par]['unit'] = u'°C'

                # calculate step valid time...
                t = g.validDate
                ## WARNING: the valid time of step,
                ##    can be differently implemented in some forecasts,
                ##    this works with ecmwf, Harmonie and IGB data...
                ##    TODO: It may be necessary to add some logic here
                ##          for other fc model outputs like NCEP models
                t += timedelta(hours = g.step * g.stepUnits)
                
                # append data
                times = data[par]['times']
                values = data[par]['values']
                times.append(t)

                # process ndarray data storage options,
                #   e.g. mmap or compressed
                #   note, storage options set with setStore()
                vals = S(vals)
                values.append(vals)

        # close file
        G.close()

    # Sorting and consistency checks here...
    # sort time steps,
    # data should be returned in correct order
    logging.info('time sorting loaded parameters')
    for par in data:
        times = data[par]['times']
        values = data[par]['values']
        res = sort_together((times, values)) # give sorted tuples
        data[par]['times'] = list(res[0]) # back to list
        data[par]['values'] = list(res[1])

    # consistency checks on loaded data,
    check_consistency(data)

    return data

# collect a single appropriate msg from grib forecast,
# for later use as template in writing snow drift results 
def getTemplateMsg(file, config):
    logging.info("collecting template msg from %r"%file)

    try:
        G = pygrib.open(file)
    except OSError as e:
        logging.error("failed to open file %r"%file)
        raise e

    # scan for appropriate msg
    for g in G:
        # read the msg identifiers
        toLevel = g['typeOfLevel']
        level = g['level']
        ioPar = g['indicatorOfParameter']
        unit = g['units']
        # construct string unique identity
        id = "%s:%d:%d"%(toLevel, level, ioPar)

        # check against config, 
        # use first temp msg as template
        if id == config['temp']['id']:
            G.close()
            logging.info(" - OK")
            return g

    # if here failed to find template msg
    G.close()
    logging.error(" - FAIL")
    raise IOError("could not find a template msg in %r"%file)

def check_consistency(data):
    # Consistency checks, e.g. no. steps and time stamps
    logging.info('performing data consistency checks')
    # check that all params and times are equivalent...
    allTimes = [data[x]['times'] for x in data]
    allN = [len(ts) for ts in allTimes]
    # must all be same length...
    logging.info(" - checking loaded params length are consistent")
    for i in range(1, len(allN)):
        prev = allN[i-1]
        curr = allN[i]
        if prev != curr:
            logging.error(" - FAIL")
            raise IOError("inconsistent number of parameter steps found")

    # check all time steps are equivalent,
    logging.info(" - checking time steps across all loaded params")
    for i in range(1, len(allTimes)):
        for k in range(len(allTimes[0])):
            if allTimes[i][k] != allTimes[i-1][k]:
                logging.error(" - FAIL")
                raise IOError("time steps dont match across all input data")
    logging.info(" - OK")

# Prints a summary of this data object,
# with an optional title
def summary(data, title="Data Summary"):
    print("-----------------------------------")
    print(title)
    print("-----------------------------------")
    mm = minmax(data)
    for k in data:
        print("  %s:"%k)
        times = data[k]['times']
        n = len(times)
        values = data[k]['values']
        unit = data[k]['unit']
        mi = mm[k]['min']
        ma = mm[k]['max']
        if n == 0:
            print("    NO DATA")
        else:
            print("    steps:       %d"%n)
            print("    min/max:     %.1f/%.1f %s"%(mi,ma,unit))
            print("    start/stop:  %s/%s"%(times[0],times[-1]))
        if 'assim' in data[k]:
            print("    assim:       %s"%data[k]['assim'])
        if isinstance(values[0], Compress):
            print("    dtype:       compressed array")
        elif isinstance(values[0], MArray):
            print("    dtype:       memmap array")
        elif isinstance(values[0], np.ndarray):
            print("    dtype:       ndarray")
        else:
            print("    dtype:       UNKNOWN")
    print("-----------------------------------")

def minmax(data):
    mm = {}
    for k in data:
        vs = data[k]['values']
        # if empty...
        if len(vs) == 0:
            mm[k] = {'min': np.nan, 'max': np.nan}
            continue
        if k not in mm:
            mm[k] = {'min': vs[0].min(), 'max': vs[0].max()}
        
        for v in vs:
            mi = mm[k]['min']
            ma = mm[k]['max']
            tma = v.max()
            tmi = v.min()
            if tma > ma:
                mm[k]['max'] = tma
            if tmi < mi:
                mm[k]['min'] = tmi
    return mm




