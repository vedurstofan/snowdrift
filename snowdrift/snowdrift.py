import logging
from .collect_data import summary, check_consistency
from .array_store import arrayStore as S
import sys

# Default model thresholds
modelTuning = {
    'snow_cover_threshold': 1.0,
    'snow_fall_threshold': 0.1,
    'wind_speed_normalization': 12.0,
    'wind_zero_drift': 6.0,
    'mobility_snowage_reduction': {
        'snowage': 24,
        'mobility': 0.6
    },
    'mobility_driftac_reduction': {
        'driftac1': 2.0,
        'mobility1': 0.6,
        'driftac2': 6.0,
        'mobility2': 0.3
    }
}

# Does in place assimilation on data,
# from input assim dataset (previous snowdrift output)
# Assimilation:
#   1. adds -1 forecast step from assim:
#            temp, wind, snowground, snowfall
#            drift, snowage, driftac, mobility
#   2. populates 0 step from assim for:
#            snowfall (because otherwise 0.0)
def assimilate(data, assim):
    logging.info("assimilating previous snowdrift forecast")
    
    # check assim data has correct keys:
    keys = sorted(assim.keys())
    shouldHave = ['drift', 'driftac', 'mobility', 'snowage', 'snowfall', 'snowground', 'temp', 'wind']
    if keys != shouldHave:
        logging.error(" - FAIL")
        raise IOError("missing forecast parameter in assimilation dataset, should have %r"%shouldHave)
    
    # lookup start time and -1 time step in assim data
    dt = get_times(data)[1] - get_times(data)[0]
    t0 = get_times(data)[0]
    assimTimes = get_times(assim)
    try:
        i0 = assimTimes.index(t0)
        im1 = assimTimes.index(t0-dt)
        tm1 = assimTimes[im1]
    except:
        logging.error(" - FAIL")
        raise IOError("failed to look up start time and/or -1 step in assimilation data set")
    
    # populate snowfall at 0 step,
    #     because don't want 0.0 snowfall on start
    data['snowfall']['values'][0] = assim['snowfall']['values'][i0]
    
    # prepend t0 - 1 step to data from assim data:
    for k in keys:
        if k not in data:
            data[k] = {'values': [], 'times': [], 'unit': assim[k]['unit']}
        data[k]['values'].insert(0, assim[k]['values'][im1])
        data[k]['times'].insert(0, assim[k]['times'][im1])
        # mark this data section as assimilated:
        data[k]['assim'] = True

    summary(data)

    # assimilation success...
    logging.info(" - OK")


# the main snow drift algorithm on loaded data
def snowdrift(data, config=modelTuning, assim=None):
    # if tuning config provided, 
    # apply it globally here to above default
    if config:
        modelTuning.update(config)

    # if have assimilation dataset,
    # execute assimilation procedure
    hasAssim = False
    if assim:
        assimilate(data, assim)
        # keep track of assimilation
        hasAssim = True
    
    # do snow drift algorithm, in steps,
    # because snowage, driftac, mobility and drift
    # are interdependent calculations,
    logging.info("snow drift algorithm")

    start = 0
    if hasAssim:
        start = 1
        logging.info(" - assimilation in effect, starting algorithm at step 1")

    for i in range(start, nsteps(data)):
        logging.info(" - snowdrift calculation step %d"%i)
        calculate_snow_cover_age(data, i)
        calculate_drift_accumulation(data, i)
        calculate_mobility_index(data, i)
        calculate_drift(data, i)

    # if data is assimilated, remove the -1 step
    if hasAssim:
        logging.info(" - removing time -1 assimilation step")
        for k in data.keys():
            data[k]['values'].pop(0)
            data[k]['times'].pop(0)

    # return results
    return data

# The drift value calculated here
def calculate_drift(data, i):

    if i == 0:
        data['drift'] = {'times':[], 'values':[], 'unit': ""}

    drift = data['drift']['values']
    drifttimes = data['drift']['times']
    
    t = get_times(data)[i]
    wind = data['wind']['values'][i]
    mi = data['mobility']['values'][i]

    windNorm = modelTuning['wind_speed_normalization']**3.0
    d = mi*(wind**3.0)/windNorm

    # set to zero if wind < 6.0
    windZeroDrift = modelTuning['wind_zero_drift']
    d[wind<windZeroDrift] = 0.0

    # set snow free areas to -1
    # mainly just for better plotting
    isc = is_snow_covered(data, i)
    d[~isc] = -1

    # append
    drift.append( S(d) )
    drifttimes.append(t)

# Mobility index/factor is 1 after new snowfall, but
# decreases...
def calculate_mobility_index(data, i):

    # input
    times = get_times(data)

    if i == 0:
        data['mobility'] = {'times':[], 'values':[], 'unit': ""}

    mobility = data['mobility']['values']
    mobilitytimes = data['mobility']['times']

    if i == 0:
        # TODO: get previous FC mobility
        # if no prev run data, initialize mobility index (with zeros)
        mi = new_array(data)
        mobility.append( S(mi) )
        mobilitytimes.append(times[0])
    else:
        mi = mobility[i-1].copy() # make copy of prev step

        # if new snow, reset mobility to 1.0
        new = is_new_snow(data, i)
        mi[new] = 1.0

        # apply snow cover age reduction...
        mAge = modelTuning['mobility_snowage_reduction']['snowage']
        mVal = modelTuning['mobility_snowage_reduction']['mobility']
        age = data['snowage']['values'][i]
        mi[ (mi > mVal)&(age >= mAge) ] = mVal

        # apply drift accumulation reduction,
        mD1 = modelTuning['mobility_driftac_reduction']['driftac1']
        mV1 = modelTuning['mobility_driftac_reduction']['mobility1']
        mD2 = modelTuning['mobility_driftac_reduction']['driftac2']
        mV2 = modelTuning['mobility_driftac_reduction']['mobility2']
        dacc = data['driftac']['values'][i]
        # NOTE: here we must ensure only reduction is applied...
        #       (mi > value) safeguard was missing in previous scripts
        mi[ (mi > mV1)&(dacc >= mD1) ] = mV1   # [2.0 6.0] range
        mi[ (mi > mV2)&(dacc > mD2) ] = mV2    # > 6.0 range

        # melting and snow cover nulling,
        melting = is_melting_temp(data, i)
        isc = is_snow_covered(data, i)
        mi[melting] = 0.0
        mi[~isc] = 0.0

        # append,
        mobility.append( S(mi) )
        mobilitytimes.append(times[i])

# Drift accumulation keeps track of quantity of accumulated drifting
# DA increases by SDValue when wind[i-1] >= 6 m/s.
# reset accumulation to 0 when new snow
def calculate_drift_accumulation(data, i):

    # input
    snowfall = data['snowfall']['values']
    times = get_times(data)

    if i == 0:
        data['driftac'] = {'times':[], 'values':[], 'unit':''}

    driftac = data['driftac']['values']
    driftactimes = data['driftac']['times']

    if i == 0:
        # here we set the first step,
        # zero by default...
        # TODO: should be loaded from prev forecast
        dacc = new_array(data)
    else:
        # prev wind, drift, drif accumulation
        pWind = data['wind']['values'][i-1]
        pDrift = data['drift']['values'][i-1]
        pDacc = data['driftac']['values'][i-1]
        dacc = pDacc.copy() # copy from previous
        # accumulate drift with wind >= 6.0
        # only add up drift where positive drift,
        # and above drift wind limit
        windZeroDrift = modelTuning['wind_zero_drift']
        windy = (pWind >= windZeroDrift) & (pDrift >= 0.0)
        dacc[windy] = pDacc[windy] + pDrift[windy]
        # reset accumulation when new snow,
        new = is_new_snow(data, i)
        dacc[new] = 0.0

    # append result,
    driftac.append( S(dacc) )
    driftactimes.append(times[i])

# Rules for aging:
# if no snow cover -> -1  # no age value
# if new snow fall -> 0 age
# else age snow by 1 fc time step (in hours)
#
# NOTE: snow cover age, does not keep track of meltin
#       this is done in mobility index,
#       i.e. melting, will turn off mobility of the top layer
def calculate_snow_cover_age(data, i):

    times = get_times(data)

    if i == 0:
        unit = "hours"
        data['snowage'] = {'times':[], 'values':[], 'unit':unit}

    snowage = data['snowage']['values']
    snowagetimes = data['snowage']['times']

    if i == 0:
        # here we set the first age step,
        # TODO: should be loaded from prev forecast
        # now just start from zero...
        isc = is_snow_covered(data, 0)
        age = new_array(data) - 1.0 # init -1
        age[isc] = 0
        snowage.append( S(age) )
        snowagetimes.append(times[0])
    else:
        # create age holder, 
        age = new_array(data) - 1.0 # init -1

        # booleans...
        isc = is_snow_covered(data, i)
        new = is_new_snow(data, i)

        # age up snow from previous step,
        dtHours = (times[i] - times[i-1]).total_seconds()/3600.0
        age[isc] = snowage[i-1][isc] + dtHours

        # new snow fall, reset age to 0
        age[new] = 0.0
        
        # no snow cover, set age to -1
        age[~isc] = -1.0

        # append data,
        snowage.append( S(age) )
        snowagetimes.append(times[i])

# calculate boolean if new snowfall in this step,
def is_new_snow(data, i):
    return data['snowfall']['values'][i] > modelTuning['snow_fall_threshold']

# calculate boolean if ground snow covered,
def is_snow_covered(data, i):
    return data['snowground']['values'][i] > modelTuning['snow_cover_threshold']

# calculate boolean for top snow is melting
def is_melting_temp(data, i):
    return data['temp']['values'][i] > 0.0

# get new empty data array to work with,
def new_array(data):
    return data['temp']['values'][0]*0.0

# data set length,
def nsteps(data):
    return len(data['temp']['times'])

def get_times(data):
    return data['temp']['times']