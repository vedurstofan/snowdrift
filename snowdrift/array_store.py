import numpy as np
from .compress import Compress
from .mmap import MArray

# Global setting adjusted by setStore()
_mmap = None
_compress = False

def setStore(mmap=None, compress=False):
    global _mmap
    global _compress
    _mmap = mmap
    _compress = compress

# Takes care of ndarray storage,
#   either compressed in RAM or
#   memmapped to disk
# If neither mmap or compressed is set,
# simply returns original ndarray object
# Note: mmap overrides compress storage.
def arrayStore(vals):
    # if vals are MArray, get the ndarray data
    if isinstance(vals, MArray):
        vals = np.array(vals.data)
    elif isinstance(vals, Compress):
        vals = vals.ca[:]

    if _mmap is not None:
        # create mmap array inside 'mmap/' directory
        return MArray(vals, _mmap, mode='w+', dtype=vals.dtype, shape=vals.shape)
    elif _compress is True:
        return Compress(vals)
    # else just return the
    # unhandled ndarray data...
    return vals
