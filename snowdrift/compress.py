# NOTE: bcolz.carray is useful for compressing
#       large numpy array data
from bcolz import carray, eval
import numbers
import numpy as np

# A simple wrapper around bcolz carray,
# mainly to abstract the math evaluations
# so that they behave like ndarray...
class Compress(object):
    def __init__(self, array):
        if isinstance(array, carray):
            # just set the data and return
            self.ca = array
            return
        # else assume ndarray or other...
        self.ca = carray(array)

    def copy(self):
        return Compress(self.ca[:])

    def __repr__(self):
        return repr(self.ca)

    def __getitem__(self, slice):
        slice = formatMath(slice)
        return Compress(self.ca[:][slice])

    def __setitem__(self, slice, new_values):
        a = self.ca[:] # expand
        slice = formatMath(slice)
        vals = formatMath(new_values)
        a[slice] = vals
        self.ca = carray(a)

    def __invert__(self):
        a = self.ca[:] # expand
        return Compress(~a)

    def __and__(self, b):
        a = self.ca[:] # expand
        b = formatMath(b)
        return Compress(a & b)

    def __eq__(self, b):
        a = self.ca
        b = formatMath(b)
        return Compress(eval("a == b"))

    def __ne__(self, b):
        a = self.ca
        b = formatMath(b)
        return Compress(eval("a != b"))

    def __lt__(self, b):
        a = self.ca
        b = formatMath(b)
        return Compress(eval("a < b"))

    def __le__(self, b):
        a = self.ca
        b = formatMath(b)
        return Compress(eval("a <= b"))

    def __gt__(self, b):
        a = self.ca
        b = formatMath(b)
        return Compress(eval("a > b"))

    def __ge__(self, b):
        a = self.ca
        b = formatMath(b)
        return Compress(eval("a >= b"))

    def __mul__(self, b):
        a = self.ca
        b = formatMath(b)
        return Compress(eval("a * b"))

    def __sub__(self, b):
        a = self.ca
        b = formatMath(b)
        return Compress(eval("a - b"))

    def __add__(self, b):
        a = self.ca
        b = formatMath(b)
        return Compress(eval("a + b"))
 
    def __truediv__(self, b):
        a = self.ca
        b = formatMath(b)
        return Compress(eval("a / b"))

    def __pow__(self, b):
        a = self.ca
        b = float(b)
        return Compress(eval("a**b"))

    def min(self):
        # must expand carray to ndarray
        return (self.ca[:]).min()

    def max(self):
        # must expand carray to ndarray
        return (self.ca[:]).max()

# checks and picks out value for math operations on Compress,
# e.g. need support numbers, Compress and ndarray
def formatMath(b):
    if isinstance(b, numbers.Number):
        return b
    elif isinstance(b, Compress):
        return b.ca[:]
    elif isinstance(b, np.ndarray):
        return b
    elif isinstance(b, tuple):
        return b
    # else raise not supported,
    raise ValueError("math operation on Compress not supported for type, %r"%type(b))

# if is Compress, return values as ndarray
def compress2Ndarray(a):
    if isinstance(a, Compress):
        return a.ca[:]
    return a
