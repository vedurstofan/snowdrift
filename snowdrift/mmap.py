import numpy as np
import uuid
import numbers
from os import path, remove
from .compress import Compress

# limit on number open memmaps,
# because sometimes num open files 
# limited by the OS
OPEN_LIMIT = 40
open_buffer = []

def add_open(ma):
    global open_buffer
    # check if need close and remove any
    # open MArrays form open buffer
    if len(open_buffer) >= OPEN_LIMIT:
        open_buffer.pop(0).close()
    # append this array...
    open_buffer.append(ma)

# memmapped array with
# automatic cleanup of memmap file
# when garbage collected...
#   using composition rather than subclassing
#   for more detailed control of operations
#   on the data...
class MArray(object):
    # here we just initialize the 
    # memmap ndarray object with the values
    def __init__(self, vals, mmap, *args, **kwargs):
        # create unique mmap filename...
        id = uuid.uuid4().hex
        filename = path.join(mmap, id)
        self.filename = filename
        self.data = np.memmap(filename, *args, **kwargs)
        self.shape = self.data.shape
        self.dtype = self.data.dtype
        self.data[:] = vals[:]
        # this marray is default open...
        # register this in the open buffer,
        # note: this will take care of closing
        #       other marrays if too many open
        add_open(self)

    # this is mainly called by
    # open buffer handler add_open() above...
    # this is non-opt if already closed
    def close(self):
        if self.data is not None:
            self.data._mmap.close()
            self.data = None

    # this is non-opt if already open,
    # but also calls open buffer handler
    # to free up other open arrays if
    # necessary...
    def open(self):
        if self.data is None:
            self.data = np.memmap(self.filename, dtype=self.dtype, shape=self.shape)
            # keep track of open arrays...
            # in open buffer
            add_open(self)

    def __del__(self):
        self.close()
        del self.data
        if self.filename is not None:
            remove(self.filename)

    def copy(self):
        self.open()
        return np.array(self.data[:])

    def __repr__(self):
        self.open()
        return repr(self.data)

    def __getitem__(self, slice):
        self.open()
        return np.array(self.data[slice])

    def __setitem__(self, slice, new_values):
        self.open()
        self.data[slice] = new_values

    def __invert__(self):
        self.open()
        return ~np.array(self.data)

    def __and__(self, b):
        self.open()
        b = formatOp(b)
        return np.array(self.data) & b

    def __eq__(self, b):
        self.open()
        b = formatOp(b)
        return np.array(self.data) == b

    def __ne__(self, b):
        self.open()
        b = formatOp(b)
        return np.array(self.data) != b

    def __lt__(self, b):
        self.open()
        b = formatOp(b)
        return np.array(self.data) < b

    def __le__(self, b):
        self.open()
        b = formatOp(b)
        return np.array(self.data) <= b

    def __gt__(self, b):
        self.open()
        b = formatOp(b)
        return np.array(self.data) > b

    def __ge__(self, b):
        self.open()
        b = formatOp(b)
        return np.array(self.data) >= b

    def __mul__(self, b):
        self.open()
        b = formatOp(b)
        return np.array(self.data) * b

    def __sub__(self, b):
        self.open()
        b = formatOp(b)
        return np.array(self.data) - b

    def __add__(self, b):
        self.open()
        b = formatOp(b)
        return np.array(self.data) + b
 
    def __truediv__(self, b):
        self.open()
        b = formatOp(b)
        return np.array(self.data) / b

    def __pow__(self, b):
        self.open()
        b = formatOp(b)
        return np.array(self.data)**b

    def min(self):
        self.open()
        # must expand carray to ndarray
        return self.data.min()

    def max(self):
        self.open()
        # must expand carray to ndarray
        return self.data.max()

# formats operand so that supported
# by ndarray above, .e.g convert MAarray
def formatOp(b):
    if isinstance(b, numbers.Number):
        return b
    elif isinstance(b, MArray):
        # make sure is open...
        b.open()
        return np.array(b.data)
    elif isinstance(b, Compress):
        return b.ca[:]
    elif isinstance(b, np.ndarray):
        return b
    elif isinstance(b, tuple):
        return b
    # else raise not supported,
    raise ValueError("math operation on MArray not supported for type, %r"%type(b))
