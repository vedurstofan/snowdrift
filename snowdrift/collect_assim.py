import logging
from .collect_data import collectData, inputConfig

# Loads assimilation data from a previous snowdrift GRIB
# forecast result. The data loading requires an
# OutputParameters configuration section in order to
# identify the correct snowdrift forecast parameters to load.
def collectAssim(driftFile, outputConfig):
    logging.info("loading assimilation data - previous snowdrift run")

    # construct config for reading prev snowdrift fc
    config = {}
    snowage = config['snowage'] = {}
    mobility = config['mobility'] = {}
    drift = config['drift'] = {}
    driftac = config['driftac'] = {}
    
    temp = config['temp'] = {}
    snowground = config['snowground'] = {}
    snowfall = config['snowfall'] = {}
    wind = config['wind'] = {}

    level = outputConfig['level']
    typeOfLevel = outputConfig['type_of_level']
    snowageParameter = outputConfig['snowage_parameter']
    mobilityParameter = outputConfig['mobility_parameter']
    driftParameter = outputConfig['drift_parameter']
    driftacParameter = outputConfig['driftac_parameter']

    tempParameter = outputConfig['temp_parameter']
    snowgroundParameter = outputConfig['snowground_parameter']
    snowfallParameter = outputConfig['snowfall_parameter']
    windParameter = outputConfig['wind_parameter']

    snowage['id'] = "%s:%d:%d"%(typeOfLevel, level, snowageParameter)
    snowage['unit'] = 'hours'
    mobility['id'] = "%s:%d:%d"%(typeOfLevel, level, mobilityParameter)
    mobility['unit'] = 'none'
    drift['id'] = "%s:%d:%d"%(typeOfLevel, level, driftParameter)
    drift['unit'] = 'none'
    driftac['id'] = "%s:%d:%d"%(typeOfLevel, level, driftacParameter)
    driftac['unit'] = 'none'

    temp['id'] = "%s:%d:%d"%(typeOfLevel, level, tempParameter)
    temp['unit'] = '°C'
    snowground['id'] = "%s:%d:%d"%(typeOfLevel, level, snowgroundParameter)
    snowground['unit'] = 'kg m**-2'
    snowfall['id'] = "%s:%d:%d"%(typeOfLevel, level, snowfallParameter)
    snowfall['unit'] = 'kg m**-2/h'
    wind['id'] = "%s:%d:%d"%(typeOfLevel, level, windParameter)
    wind['unit'] = 'm s**-1'

    # load the assimilation forecast data
    data = collectData([driftFile], config=config)
    return data