import unittest
import snowdrift
        
class Test(unittest.TestCase):
    
    @classmethod
    def setUpClass(self):
        pass

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_default_config(self):
        # no config file argument set...
        # so should default
        conf = snowdrift.getConfig()
        inputPars = sorted(conf['InputParameters'].keys())
        modelTuning = sorted(conf['ModelTuning'].keys())
        outputPars = sorted(conf['OutputParameters'].keys())

        self.assertEqual(inputPars, ['snowac', 'snowground', 'temp', 'wind-u', 'wind-v'])
        self.assertEqual(modelTuning, ['mobility_driftac_reduction',
            'mobility_snowage_reduction',
            'snow_cover_threshold',
            'snow_fall_threshold',
            'wind_speed_normalization',
            'wind_zero_drift'])
        self.assertEqual(outputPars, 
            ['drift_parameter', 
            'driftac_parameter', 
            'level', 
            'mobility_parameter', 
            'snowage_parameter', 
            'snowfall_parameter', 
            'snowground_parameter', 
            'temp_parameter', 
            'type_of_level', 
            'wind_parameter'])

    def test_config_file(self):
        conf = snowdrift.getConfig("tests/testConf1.yml")
        inputPars = sorted(conf['InputParameters'].keys())
        modelTuning = sorted(conf['ModelTuning'].keys())
        outputPars = sorted(conf['OutputParameters'].keys())
        
        self.assertEqual(conf['OutputParameters']['driftac_parameter'], 166)
        self.assertEqual(conf['ModelTuning']['snow_cover_threshold'], 1.5)
        self.assertEqual(inputPars, ['snowac', 'snowground', 'temp', 'wind-u', 'wind-v'])
        self.assertEqual(modelTuning, ['mobility_driftac_reduction',
            'mobility_snowage_reduction',
            'snow_cover_threshold',
            'snow_fall_threshold',
            'wind_speed_normalization',
            'wind_zero_drift'])
        self.assertEqual(outputPars, 
            ['drift_parameter', 
            'driftac_parameter', 
            'level', 
            'mobility_parameter', 
            'snowage_parameter', 
            'snowfall_parameter', 
            'snowground_parameter', 
            'temp_parameter', 
            'type_of_level', 
            'wind_parameter'])

    def test_config_file_missing_sections(self):
        conf = snowdrift.getConfig("tests/testConf2.yml")
        inputPars = sorted(conf['InputParameters'].keys())
        modelTuning = sorted(conf['ModelTuning'].keys())
        outputPars = sorted(conf['OutputParameters'].keys())
        
        self.assertEqual(inputPars, ['snowac', 'snowground', 'temp', 'wind-u', 'wind-v'])
        self.assertEqual(modelTuning, ['mobility_driftac_reduction',
            'mobility_snowage_reduction',
            'snow_cover_threshold',
            'snow_fall_threshold',
            'wind_speed_normalization',
            'wind_zero_drift'])
        self.assertEqual(outputPars, 
            ['drift_parameter', 
            'driftac_parameter', 
            'level', 
            'mobility_parameter', 
            'snowage_parameter', 
            'snowfall_parameter', 
            'snowground_parameter', 
            'temp_parameter', 
            'type_of_level', 
            'wind_parameter'])
