import unittest
import os
import urllib.request
import time
import snowdrift

def get_data():
    # if testdata dir does not exist, then get testdata from cloud
    if not os.path.isdir("tests/testdata"):
        print("\nGetting testdata:")
        urllib.request.urlretrieve("https://s3.eu-west-2.amazonaws.com/share.bitvinci.is/testdata/snowdrift_testdata.zip", "tests/testdata.zip")
        # extract the zipped data
        os.system("cd tests; unzip testdata.zip")
        
class Test(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        # get_data will get test data if 'testdata' dir
        # is not found in test/ dir
        get_data()
        # load full testdata set
        self.files = [
            'tests/testdata/harmonie_2021032306.00', 
            'tests/testdata/harmonie_2021032306.03',
            'tests/testdata/harmonie_2021032306.06',
            'tests/testdata/harmonie_2021032306.01',
            'tests/testdata/harmonie_2021032306.04',
            'tests/testdata/harmonie_2021032306.02',
            'tests/testdata/harmonie_2021032306.05'
        ]
        self.files3 = [
            'tests/testdata/harmonie_2021032306.00',
            'tests/testdata/harmonie_2021032306.01',
            'tests/testdata/harmonie_2021032306.02',
        ]

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_input_data_loading(self):
        # check that we have loaded parameters:
        # temp, snowac, snowground, temp, wind-u/v
        data = snowdrift.collectData(self.files3)
        params = sorted(data.keys())
        self.assertEqual(params, ['snowac', 'snowground', 'temp', 'wind-u', 'wind-v'])

    def test_process_input_data(self):
        # process should calculate snowfall and wind,
        data = snowdrift.collectData(self.files3)
        snowdrift.processInputData(data)
        self.assertIn('snowfall', data)
        self.assertIn('wind', data)
        self.assertNotIn('wind-u', data)
        self.assertNotIn('wind-v', data)
        self.assertNotIn('snowac', data)

    def test_snowdrift(self):
        # snowdrift algorithm should add,
        # snowage, mobility, driftac, drift parameters,
        data = snowdrift.collectData(self.files3)
        snowdrift.processInputData(data)
        snowdrift.snowdrift(data)
        self.assertIn('snowage', data)
        self.assertIn('mobility', data)
        self.assertIn('driftac', data)
        self.assertIn('drift', data)

    def test_input_data_sorting(self):
        # order of forecast data input should not affect
        # data loading...
        files1 = [
            'tests/testdata/harmonie_2021032306.00',
            'tests/testdata/harmonie_2021032306.01',
            'tests/testdata/harmonie_2021032306.02',
        ]
        files2 = [
            'tests/testdata/harmonie_2021032306.01',
            'tests/testdata/harmonie_2021032306.00',
            'tests/testdata/harmonie_2021032306.02',
        ]
        data1 = snowdrift.collectData(files1)
        data2 = snowdrift.collectData(files2)
        times1 = data1['temp']['times']
        times2 = data2['temp']['times']
        self.assertEqual(times1, times2)

    def test_compression(self):
        from snowdrift import setStore

        files = [
            'tests/testdata/harmonie_2021032306.00',
            'tests/testdata/harmonie_2021032306.01',
            'tests/testdata/harmonie_2021032306.02',
            'tests/testdata/harmonie_2021032306.03',
            'tests/testdata/harmonie_2021032306.04',
        ]

        # no compression
        data1 = snowdrift.collectData(files)
        snowdrift.processInputData(data1)
        snowdrift.snowdrift(data1)

        # with compression
        setStore(compress=True)
        data2 = snowdrift.collectData(files)
        snowdrift.processInputData(data2)
        snowdrift.snowdrift(data2)

        drift1 = data1['drift']['values'][-1]
        drift2 = data1['drift']['values'][-1]

        sum1 = drift1.sum()
        sum2 = drift2.sum()

        # assert that drift sum is equal in both
        # compressed and uncompressed processing
        self.assertEqual(sum1, sum2)
        
